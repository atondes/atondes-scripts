#!/usr/bin/env bash

FILE=./wordpress-remote
if ! test -f "$FILE"; then
  mkdir $FILE
fi

sshfs atondes:/httpdocs/ ./wordpress-remote/
