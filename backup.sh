#!/usr/bin/env bash

thisDay=`date +%Y-%m-%d`
thisMonth=`date +%Y-%m`

FILE=./mysql-password
if test -f "$FILE"; then
  mysqlpassword=`cat $FILE`
else
	echo Please enter the mysql password?
	read mysqlpassword
	echo $mysqlpassword>$FILE
fi

ssh atondes '( mysqldump --user=rotondes --password="'$mysqlpassword'" rotondes > /home/db-backup/rotondes.'$thisDay'.sql )'
scp atondes:'home/db-backup/rotondes.'$thisDay'.sql' ./backup/
rsync -a --info=progress2 atondes:/httpdocs/* ./backup/wordpress.$thisDay
if hash pv 2>/dev/null; then
	tar cf - ./backup/wordpress.$thisDay -P | pv -s $(du -sb ./backup/wordpress.$thisDay | awk '{print $1}') | gzip > ./backup/wordpress.$thisDay.tar.gz
else
	tar cvzf ./backup/wordpress.$thisDay.tar.gz ./backup/wordpress.$thisDay
fi
rm -r ./backup/wordpress.$thisDay
